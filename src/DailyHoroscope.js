import React, { Component } from 'react';
import HoroscopeMarkov from './markov/HoroscopeMarkov';

class DailyHoroscope extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  renderHoroscope(sign) {
    return this.props.chain.getHoroscope("aries")
  }

  render() {
    return(
      <div>
        <h2>{this.props.sign}</h2>
        {this.renderHoroscope(this.props.sign)}
      </div>
    )
  }
}

export default DailyHoroscope;
