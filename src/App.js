import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import DailyHoroscope from './DailyHoroscope';
import HoroscopeMarkov from './markov/HoroscopeMarkov';

const horoscopes = {
  aries: [
    "Even as you’re tearing into your resolutions, hit pause to check in with your conscience. You’re a firm believer in actions speaking louder than words, and today, Ram, you can make a meaningful gesture—with your wallet! A compassionate connection between the most sensitive, tuned-in stars—the moon and Neptune—across your service-oriented axis guides you to make smart and meaningful shopping choices. Spend your hard-earned money at companies whose values you embrace, like green production policies or fair labor practices.",
    "It’s a great day to clean up your place, get organized, do some chores, and get some physical activity in!",
    ],
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      markovChains: new HoroscopeMarkov({ source: horoscopes }),
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">auto astrology</h1>
        </header>
        <p className="App-intro">
          machine informed divinity
        </p>
        <h3>Here is the horoscope for {this.state.date.toLocaleDateString()}</h3>
        <DailyHoroscope sign="Aries" chain={this.state.markovChains} />
      </div>
    );
  }
}

export default App;
