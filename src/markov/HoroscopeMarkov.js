class HoroscopeMarkov {
  constructor (options = {}) {
    this.max = 20;
    this.order = 3;
    this.beginnings = [];
    this.lookupTable = {};
    this.source = [];
  }

  getHoroscope(sign) {
    return "horoscope for " + sign
  }

}

export default HoroscopeMarkov;
